/*===========================================================================
  Copyright (C) 2010-2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.steps.rainbowkit.xliff;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.TestUtil;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.pipelinedriver.BatchItemContext;
import net.sf.okapi.common.pipelinedriver.IPipelineDriver;
import net.sf.okapi.common.pipelinedriver.PipelineDriver;
import net.sf.okapi.filters.properties.PropertiesFilter;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;
import net.sf.okapi.steps.rainbowkit.creation.ExtractionStep;
import net.sf.okapi.steps.rainbowkit.creation.Parameters;
import net.sf.okapi.steps.xsltransform.XSLTransformStep;

import org.custommonkey.xmlunit.SimpleNamespaceContext;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.custommonkey.xmlunit.exceptions.XpathException;

import static org.junit.Assert.assertTrue;
import org.junit.runners.JUnit4;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

@RunWith(JUnit4.class)
public class ExtractionStepTest {
	
	private String root;
	private LocaleId locEN = LocaleId.fromString("en");
	private LocaleId locFR = LocaleId.fromString("fr");
	
	@Before
	public void setUp() {
		root = TestUtil.getParentDir(this.getClass(), "/test01.properties");
		// Java returns paths with a leading slash even on Windows; this
		// does not represent what will really be passed to the step in practice.
		// Strip the leading slash if we're on Windows with a drive-letter path.
		if (System.getProperty("os.name").startsWith("Windows") && Pattern.matches("^/[A-Z]:.*$", root))
			root = root.substring(1);
	}
	
	@Test
	public void testSimpleStep ()
		throws URISyntaxException, XpathException, FileNotFoundException, SAXException, IOException
	{
		// Ensure output is deleted
		assertTrue(Util.deleteDirectory(new File(root+"packXliff1")));
		
		IPipelineDriver pdriver = new PipelineDriver();
		FilterConfigurationMapper fcMapper = new FilterConfigurationMapper();
		fcMapper.addConfigurations(PropertiesFilter.class.getName());
		pdriver.setFilterConfigurationMapper(fcMapper);
		pdriver.setRootDirectories(Util.deleteLastChar(root), Util.deleteLastChar(root)); // Don't include final separator
		pdriver.addStep(new RawDocumentToFilterEventsStep());
		
		ExtractionStep es = new ExtractionStep();
		Parameters params = (Parameters) es.getParameters();
		params.setWriterClass(XLIFFPackageWriter.class.getName());
		params.setSendOutput(true);
		params.setPackageName("packXliff1");
		pdriver.addStep(es);

		XSLTransformStep xslTransformStep = new XSLTransformStep();
		net.sf.okapi.steps.xsltransform.Parameters xsltParams = (net.sf.okapi.steps.xsltransform.Parameters)xslTransformStep.getParameters();
		xsltParams.setXsltPath(root+"/addCustomAttribute.xsl");
		xsltParams.setPassOnOutput(true);
		pdriver.addStep(xslTransformStep);
		
		String inputPath = root+"/test01.properties";
		String outputPath = inputPath.replace("test01.", "test01.out.");
		URI inputURI = new File(inputPath).toURI();
		URI outputURI = new File(outputPath).toURI();
		pdriver.addBatchItem(new BatchItemContext(inputURI, "UTF-8", "okf_properties", outputURI, "UTF-8", locEN, locFR));

		pdriver.processBatch();

		//verify that the extraction step did its work
		File file = new File(root+"packXliff1/work/test01.properties.xlf");
		assertTrue(file.exists());
		
		//verify that the xslt transform step did its work
		Map<String, String> nsPrefixes = new HashMap<>();
		nsPrefixes.put("custom", "custom-uri");
		nsPrefixes.put("xlf", "urn:oasis:names:tc:xliff:document:1.2");
		XMLUnit.setXpathNamespaceContext(new SimpleNamespaceContext(nsPrefixes));
		XMLAssert.assertXpathEvaluatesTo("custom-val", "/xlf:xliff/xlf:file/@custom:custom-attribute",  
				new InputSource(new FileInputStream(file)));
	}
	
    public boolean deleteOutputDir (String dirname, boolean relative) {
    	File d;
    	if ( relative ) d = new File(root + File.separator + dirname);
    	else d = new File(dirname);
    	if ( d.isDirectory() ) {
    		String[] children = d.list();
    		for ( int i=0; i<children.length; i++ ) {
    			boolean success = deleteOutputDir(d.getAbsolutePath() + File.separator + children[i], false);
    			if ( !success ) {
    				return false;
    			}
    		}
    	}
    	if ( d.exists() ) return d.delete();
    	else return true;
    }
}
